//
//  CatalogModel.swift
//  catalog
//
//  Created by developer on 10/23/15.
//  Copyright © 2015 developer. All rights reserved.
//

import Foundation
import UIKit

struct Category {
  var category_id: String
  var name : String
  var description : String
  var image : UIImage
  var children : [Category]

  init (category_id:String, name: String, description: String, image: UIImage, children: [Category]) {
    self.category_id = category_id
    self.name = name
    self.description = description
    self.image = image
    self.children = children
  }
  
}

//DB

/*
var cat1 = Category(name:"Sensors", description:"demo-desc", image:"http://www.autonicsonline.com/images/n28_prd.jpg", children: ["proximary sensors", "door sensors"], count: 50)
var cat2 = Category(name:"Controllers", description:"demo-controller", image:"http://www.autonicsonline.com/images/n28_prd.jpg", children: ["proximary sensors", "door sensors"], count: 50)

let categoriesData = [cat1,cat2]
*/


class CategoryModel {

  var categoriesData:[Category] = []

  //TODO. use direct CURL than async call, besso. FAST!!
  func callTask( callback:[Category] -> Void ) {
    
    var categories:[Category] = []
    var subCategories:[Category] = []
    
    // TODO. move constants as one header file.
    let sRoute = "feed/rest_api/categories"
    let sKey = "tcTznFpxjTAW8vnq"
    
    let params = "?route=\(sRoute)&key=\(sKey)"
    
    let sUrl = "http://autonics-ec.dev/index.php" + params
    let oUrl = NSURL(string: sUrl)!
    
    let data = try? NSData(contentsOfURL: oUrl, options: [])
    let oJson = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers ) as? NSDictionary
    let cats = oJson!["categories"] as! NSArray
    
    for cat in cats {
      let category = cat as! NSDictionary
      
      let category_id = category["category_id"] as! String
      let name = category["name"] as! String
      let description = category["description"] as! String
      let imageData = NSData(contentsOfURL: NSURL(string: category["image"] as! String)!)
      let image = UIImage(data: imageData!)
      
      
      
      let children = category["children"] as! NSArray
      
      
      if children.count > 0 {
        subCategories = []
        for child in children {
          
          let subCategory = child as! NSDictionary
          
          let sub_category_id = subCategory["category_id"] as! String
          let sub_name = subCategory["name"] as! String
          let sub_description = subCategory["description"] as! String
          let sub_imageData = NSData(contentsOfURL: NSURL(string: subCategory["image"] as! String)!)
          let sub_image = UIImage(data: sub_imageData!)
          
          let sub_children:[Category] = []
          
          let tmpSubCategory = Category(category_id:sub_category_id, name:sub_name, description:sub_description, image:sub_image!, children:sub_children )
          subCategories.append(tmpSubCategory)
          
        }
      }
      
      categories.append(Category(category_id:category_id, name:name, description:description, image:image!, children:subCategories))
      
      
      callback(categories)
      
    }
    
    //taskGetCategories.resume()
    
    
  }
  
  /* async call is quite slow, besso
  func callTask( callback:[Category] -> Void ) {
    // TODO. move constants as one header file.
    let sRoute = "feed/rest_api/categories"
    let sKey = "tcTznFpxjTAW8vnq"
    
    let params = "?route=\(sRoute)&key=\(sKey)"

    let sUrl = "http://autonics-ec.dev/index.php" + params
    let oUrl = NSURL(string: sUrl)!

    let session = NSURLSession.sharedSession()


    var categories:[Category] = []
    var subCategories:[Category] = []

  
    let taskGetCategories = session.dataTaskWithURL(oUrl, completionHandler: {
      (data, response, error) -> Void in
      if error != nil {
        print(error!.localizedDescription)
      } else {
        //var result = NSString(data: data!, encoding: NSUTF8StringEncoding)!
        
        let oJson = try! NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers) as! NSDictionary
        
        let cats = oJson["categories"] as! NSArray

        for cat in cats {
          let category = cat as! NSDictionary
          
          let category_id = category["category_id"] as! String
          let name = category["name"] as! String
          let description = category["description"] as! String
          let image = category["image"] as! String
          let children = category["children"] as! NSArray

          
          if children.count > 0 {
            for child in children {
              let subCategory = child as! NSDictionary
              
              let sub_category_id = subCategory["category_id"] as! String
              let sub_name = subCategory["name"] as! String
              let sub_description = subCategory["description"] as! String
              let sub_image = subCategory["image"] as! String
              let sub_children:[Category] = []

              let tmpSubCategory = Category(category_id:sub_category_id, name:sub_name, description:sub_description, image:sub_image, children:sub_children )
              subCategories.append(tmpSubCategory)

            }
          }
          
          categories.append(Category(category_id:category_id, name:name, description:description, image:image, children:subCategories))
          
          callback(categories)
          self.assignResultOutside(categories)

        }
        
        
      }
      
    })

    taskGetCategories.resume()


  }
  
  
  func assignResultOutside(categories:[Category]) {
    self.categoriesData = categories
    //print( self.categoriesData )
  }
  */

}


