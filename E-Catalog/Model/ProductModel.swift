//
//  ProductModel.swift
//  catalog
//
//  Created by developer on 10/27/15.
//  Copyright © 2015 developer. All rights reserved.
//

import Foundation
import UIKit


struct Product {
  var product_id: String
  var model : String
  var description : String
  var image : UIImage
  var options : NSDictionary?
  var price : Double?
  var inventory : Int?
  
  init (product_id: String, model: String, description: String, image: UIImage, options: NSDictionary, price: Double, inventory: Int) {
    self.product_id = product_id
    self.model = model
    self.description = description
    self.image = image
    self.options = options
    self.price = price
    self.inventory = inventory
  }
}

class ProductModel {
  
  var productsData:[Product] = []
  
  //TODO. use direct CURL than async call, besso. FAST!!
  func callTask( category_id:String, callback:[Product] -> Void ) {
    
    var products:[Product] = []
    
    // TODO. move constants as one header file.
    let sRoute = "feed/rest_api/products"
    let sKey = "tcTznFpxjTAW8vnq"
    
    let params = "?route=\(sRoute)&key=\(sKey)&category_id=\(category_id)"
    
    let sUrl = "http://autonics-ec.dev/index.php" + params
    let oUrl = NSURL(string: sUrl)!
    
    let data = try? NSData(contentsOfURL: oUrl, options: [])
    let dicProducts = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments ) as? NSDictionary
    let rows = dicProducts!["products"] as? NSArray
    
    for row in rows! {
      let p = row as! NSDictionary
      
      let product_id = p["product_id"] as! String
      let model = p["model"] as! String
      let description = p["description"] as! String
      
      var options = p["options"] as? NSDictionary
      if options == nil {
        options = [:]
      }
      var price = p["price"] as? Double
      if price == nil {
        price = 0
      }
      var inventory = p["inventory"] as? Int
      if inventory == nil {
        inventory = 0
      }
      
      // image
      var image : UIImage!
      let imageStr = p["image"] as? String
      if imageStr == nil {
        print(p["model"])
        image = UIImage(named:"no_image.png")
      } else {
        let sUrl = NSURL(string: imageStr!)
        if let imageData = NSData(contentsOfURL: sUrl! ) {
          image = UIImage(data: imageData)
        } else {
          print(p["image"])
          image = UIImage(named:"no_image.png")
        }
      }
      
      products.append(Product(product_id:product_id, model:model, description:description, image:image!, options: options!, price: price!, inventory: inventory!))
      
      callback(products)
      
    }
    
  }
}
