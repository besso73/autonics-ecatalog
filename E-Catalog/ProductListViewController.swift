//
//  ProductListViewController.swift
//  E-Catalog
//
//  Created by developer on 11/4/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

extension ProductListViewController : MKAccordionViewDelegate {
  
  func accordionView(accordionView: MKAccordionView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //print("tab A : \(accordionView.tag)")
    return 0
  }
  
  func accordionView(accordionView: MKAccordionView, heightForHeaderInSection section : Int) -> CGFloat {
    //print("tab B : \(accordionView.tag)")
    return 100
  }
  
  // cell 터치시 해당 view 를 변경하려는 것이다. 기본뷰까지 해당
  func accordionView(accordionView: MKAccordionView, viewForHeaderInSection section: Int, isSectionOpen sectionOpen: Bool) -> UIView? {
    
    print("call accordionView")
    
    //Model
    let product = self.products[section]
    
    let headerView : ProductListHeader = ProductListHeader(frame: CGRect(x:0, y:0, width:400, height:100))
    
    //let headerView : CellHeader = CellHeader(frame: CGRect(x:0, y:0, width:400, height:100))
    
    print("-------")
    print(product.image)
    print(product.model)
    print(product.description)
    
    headerView.productImageView.image = product.image
    headerView.productTitleLabel.text = product.model
    
    //    headerView.productDescLabel.numberOfLines = 0
    //    headerView.productDescLabel.sizeToFit()
    headerView.productDescLabel.text  = product.description
    
    return headerView

  }
  
}

//DataSource 를 extend 함
extension ProductListViewController : MKAccordionViewDatasource {
  
  func numberOfSectionsInAccordionView(accordionView: MKAccordionView) -> Int {
    return products.count
  }
  
  func accordionView(accordionView: MKAccordionView, numberOfRowsInSection section: Int) -> Int {
    
    
    let product = products[section] as Product
    let options = product.options!
    return options.count
  }
  
  func accordionView(accordionView: MKAccordionView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell : UITableViewCell? = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: nil)
    //cell?.imageView = UIImageView(image: UIImage(named: "lightGrayBarWithBluestripe"))
    
    // Background view
    /*****
    let bgView : UIView? = UIView(frame: CGRectMake(0, 0, CGRectGetWidth(accordionView.bounds), 50))
    let bgImageView : UIImageView! = UIImageView(image: UIImage(named: "lightGrayBarWithBluestripe"))
    bgImageView.frame = (bgView?.bounds)!
    bgImageView.contentMode = UIViewContentMode.ScaleToFill
    bgView?.addSubview(bgImageView)
    cell?.backgroundView = bgView
    ******/
    
    //print( "section is same ?? \(indexPath.row)")
    
    
    cell!.frame.size.height = 50
    // You can assign cell.selectedBackgroundView also for selected mode
    let product = products[indexPath.section] as Product
    let options = product.options!
    
    let subView : UIView = UIView (frame: CGRectMake(20,0, CGRectGetWidth(accordionView.bounds), 50))
    //subView.backgroundColor = UIColor.orangeColor()
    
    var i = 0
    for option in options {
      if ( i == indexPath.row ) {
        let optionLabel : UILabel = UILabel (frame: CGRectMake(10, 0,300,50))
        optionLabel.text = (option.key as! String) + " : " + (option.value as! String)
        subView.addSubview(optionLabel)
      }
      i++
    }
    
    
    cell?.addSubview(subView)
    
    return cell!
  }
  
}

class ProductListViewController : UITableViewController, ENSideMenuDelegate {

  //var productListViewController: ProductListViewController? = nil
  
  let cellIdentifier = "ProductListCell"
  
  var products:[Product] = []
  var category_id:String?
  
  override func viewWillAppear(animated: Bool) {
    
    super.viewWillAppear(animated)
    
    // to prepare categoryData from NSURLSession
    if category_id == nil {
      category_id = "999"
    }
    //print("Category id in ProductListVC : \(category_id)")
    ProductModel().callTask(category_id!){
      products -> Void in
      self.products = products
      self.tableView.reloadData()
    }
    
    // Detour shit but work, besso
    self.tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let accordionView : MKAccordionView = MKAccordionView(frame: CGRectMake(0, 22, CGRectGetWidth(view.bounds), CGRectGetHeight(view.bounds)));
    accordionView.delegate = self;
    accordionView.dataSource = self;
    view.addSubview(accordionView);
    
    self.sideMenuController()?.sideMenu?.delegate = self
    

  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  

  @IBAction func menuTapped(sender: AnyObject) {
    toggleSideMenuView()
  }
  
}

