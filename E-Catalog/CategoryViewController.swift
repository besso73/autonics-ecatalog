//
//  ViewController.swift
//  E-Catalog
//
//  Created by developer on 11/4/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

class CategoryTableViewController: UITableViewController, ENSideMenuDelegate  {

  var categories:[Category] = []
  
  //var subController : UITableViewController!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // for Menu Drawer library
    self.sideMenuController()?.sideMenu?.delegate = self
  
  }
  
  
  override func viewWillAppear(animated: Bool) {
    CategoryModel().callTask(){
      cats -> Void in
      self.categories = cats
      self.tableView.reloadData()
      
      
      
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
    //print("call prepareForSegue in CategoryVC : \(segue.identifier)")
    if segue.identifier == "showSubCategory" {
      if let indexPath = self.tableView.indexPathForSelectedRow {
        
        let category = categories[indexPath.row] as Category
        
        //let controller = (segue.destinationViewController as! UINavigationController).topViewController as! SubCategoryViewController
        
        let controller = segue.destinationViewController as! SubCategoryViewController
        
        controller.subCategories = category.children
        
        controller.navigationItem.title = category.name
        
        //controller.navigationItem.hidesBackButton = false
        
        
        //let backItem = UIBarButtonItem(title: "Category", style: UIBarButtonItemStyle.Plain, target: self, action: "backTapped")
        //let backItem = UIBarButtonItem(title: "Category", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        
        
        //controller.navigationItem.leftBarButtonItem = backItem
        
        //self.subController = controller
        
        //controller.navigationItem.backBarButtonItem = backItem

        // self.navigationController?.navigationBar.topItem?.backBarButtonItem
        //controller.navigationController?.navigationBar.topItem?.backBarButtonItem = backItem
        
        /*
        controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
        controller.navigationItem.leftItemsSupplementBackButton = true
        */
      }
    }
  }

//  func backTapped() {
//    print("call backTapped")
//    
//    /* not working
//    let bundle = NSBundle.mainBundle()
//    let storyboard : UIStoryboard = UIStoryboard(name:"Main", bundle:bundle)
//    let vc = storyboard.instantiateViewControllerWithIdentifier("CategoryViewController")
//    */
//    let vc = self.navigationController!.viewControllers.first!
//    self.subController.navigationController?.pushViewController(vc, animated: true)
//    
//    //self.navigationController?.pushViewController(vc, animated: true)
//    self.navigationController?.navigationItem.setLeftBarButtonItem(nil, animated: true)
//    
//    print("after pushVC")
//    
//    //self.presentViewController(self, animated: false, completion: nil)
//   
//    //let vc = self.storyboard.instantiateViewControllerWithIdentifier("CategoryViewController") as! CategoryViewController
//    
//  }
  
  // MARK: - Table View
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return categories.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    self.tableView.rowHeight = 100
    
    let cell = self.tableView.dequeueReusableCellWithIdentifier("CategoryCell", forIndexPath: indexPath)
    
    let category = categories[indexPath.row] as Category
    
    cell.textLabel?.text = category.name
    cell.detailTextLabel?.text = category.description
    
    cell.imageView?.image = category.image
    cell.imageView?.frame = CGRectMake(0,0,100,100)
    
    return cell
    
  }
  
  /* No Edit
  override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
  }
  
  override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
      categories.removeAtIndex(indexPath.row)
      tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
      // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
  }
  */
  
  @IBAction func menuTapped(sender: UIBarButtonItem) {
    
//    print("menu tapped")
//    
//    let menu = self.sideMenuController()?.sideMenu?.menuViewController as! MenuTableViewController
//    menu.foo()
    
    toggleSideMenuView()
  }
}

