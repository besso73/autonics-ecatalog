//
//  ViewController.swift
//  SwiftAndPHPSignUpExample
//
//  Created by Sergey Kargopolov on 2015-06-23.
//  Copyright (c) 2015 Sergey Kargopolov. All rights reserved.
//

import UIKit

class SigninViewController: UIViewController {
  
  
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  @IBAction func siginInButtonTapped(sender: AnyObject) {
  
    let email = emailTextField.text!
    let password = passwordTextField.text!
    
    if ( email.isEmpty || password.isEmpty ) {
      
      // Display an alert message
      let myAlert = UIAlertController(title: "Alert", message:"All fields are required to fill in", preferredStyle: UIAlertControllerStyle.Alert);
      let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:nil)
      myAlert.addAction(okAction);
      self.presentViewController(myAlert, animated: true, completion: nil)
      return
    }
    
    let loading = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    loading.labelText = "Loading"
    loading.detailsLabelText = "Please wait"
    
    // TODO. move constants as one header file.
    let sRoute = "feed/rest_api/signin"
    let sKey = "tcTznFpxjTAW8vnq"
    let params = "?route=\(sRoute)&key=\(sKey)&username=\(email)&password=\(password)"
    
    let sUrl = "http://autonics-ec.dev/index.php" + params
    let oUrl = NSURL(string: sUrl)!
    
    /*
    let request = NSMutableURLRequest(URL:myUrl!);
    request.HTTPMethod = "POST";
    
    let postString = "route=\(sRoute)&key=\(sKey)&username=\(userEmailAddress)&password=\(userPassword)";
    request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding);
    */
    
    NSURLSession.sharedSession().dataTaskWithURL(oUrl, completionHandler: {
      (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
      dispatch_async(dispatch_get_main_queue()) {
        
        loading.hide(true)
        
        if(error != nil){
          //Display an alert message
          let myAlert = UIAlertController(title: "Alert", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert);
          
          //print(error!.description)
          
          let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:nil)
          myAlert.addAction(okAction);
          self.presentViewController(myAlert, animated: true, completion: nil)
          return
        }
        
        do {
          
          let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
          
          //print(json)
          
          if let parseJSON = json {
            
            let userId = parseJSON["userId"] as? String
            

            
            if( userId != nil && userId != "" ) {
            

              
              let userDefaults = NSUserDefaults.standardUserDefaults()
              
              userDefaults.setObject(parseJSON["userFirstName"], forKey: "userFirstName")
              //userDefaults.setObject(parseJSON["userLastName"], forKey: "userLastName")
              userDefaults.setObject(parseJSON["userId"], forKey: "userId")
              userDefaults.synchronize()
              
              
              // take user to a protected page
              let bundle = NSBundle.mainBundle()
              let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: bundle)
              
              let mainPage = mainStoryboard.instantiateViewControllerWithIdentifier("CategoryViewController")
              
              self.sideMenuController()?.setContentViewController(mainPage)
              self.toggleSideMenuView()
              
              /*
              let mainPageNav = UINavigationController(rootViewController: mainPage)
              let appDelegate = UIApplication.sharedApplication().delegate
              
              appDelegate?.window??.rootViewController = mainPageNav
              */
              
              //let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
              
              //appDelegate.buildNavigationDrawer()
              
              
            } else {
              
              print("Error")
              
              // display an alert message
              let userMessage = parseJSON["message"] as? String
              // TODO. message not there
              let myAlert = UIAlertController(title: "Sign-in Fail", message: userMessage, preferredStyle: UIAlertControllerStyle.Alert);
              let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:nil)
              myAlert.addAction(okAction);
              self.presentViewController(myAlert, animated: true, completion: nil)
            }
            
          }
        } catch
        {
          print(error)
        }
        
        
      }
    }).resume()
  }
  
  @IBAction func menuTapped(sender: AnyObject) {
    toggleSideMenuView()
  }
  
}

