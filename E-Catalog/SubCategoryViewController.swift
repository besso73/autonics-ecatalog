//
//  SubCategoryViewController.swift
//  E-Catalog
//
//  Created by developer on 11/4/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

class SubCategoryViewController: UITableViewController, ENSideMenuDelegate {
  
  var subCategories:[Category] = []
  
  var childVC : UITableViewController!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.sideMenuController()?.sideMenu?.delegate = self
    
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()

  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(true)
    //self.navigationItem.hidesBackButton = false
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
    if segue.identifier == "showProductList" {
      if let indexPath = self.tableView.indexPathForSelectedRow {
        print("call prepareForSegue in SubCategoryVC : ")
        let sub = subCategories[indexPath.row] as Category
        //let controller = (segue.destinationViewController as! UINavigationController).topViewController as! ProductListViewController
        let controller = segue.destinationViewController as! ProductListViewController
        controller.category_id = sub.category_id

        controller.navigationItem.title = sub.name
        
        //controller.navigationItem.hidesBackButton = false
        
        
        //let backItem = UIBarButtonItem(title: sub.name , style: UIBarButtonItemStyle.Plain, target: self, action: "backTapped")
        //let backItem = UIBarButtonItem(title: "Category", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        
        //controller.navigationItem.leftBarButtonItem = backItem
        //self.childVC = controller
        
        //controller.navigationItem.backBarButtonItem = backItem
        
        // self.navigationController?.navigationBar.topItem?.backBarButtonItem
        //controller.navigationController?.navigationBar.topItem?.backBarButtonItem = backItem
        
        /*
        controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
        controller.navigationItem.leftItemsSupplementBackButton = true
        */
        
      }
    }
  }

  // table
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    print("SubCategories : \(subCategories.count)")
    return subCategories.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    self.tableView.rowHeight = 100
    
    let cell = self.tableView.dequeueReusableCellWithIdentifier("SubCategoryCell", forIndexPath: indexPath)
    
    let category = subCategories[indexPath.row] as Category
    cell.textLabel?.text = category.name
    cell.detailTextLabel?.text = category.description
    
    cell.imageView?.image = category.image
    cell.imageView?.frame = CGRectMake(0,0,100,100)
    
    return cell
  }
  
  override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
  }
  
  @IBAction func menuTapped(sender: AnyObject) {
    toggleSideMenuView()
  }
  
  
}
