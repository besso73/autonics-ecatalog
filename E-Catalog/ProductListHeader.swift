//
//  ProductListHeader.swift
//  catalog
//
//  Created by developer on 10/30/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

class ProductListHeader : UIView {


  @IBOutlet weak var productImageView: UIImageView!
  @IBOutlet weak var productTitleLabel: UILabel!
  @IBOutlet weak var productDescLabel: UILabel!

  var view : UIView!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    print("call init UIView")
    super.init(coder: aDecoder)
    print("Count : \(self.subviews.count)")
    if self.subviews.count == 0 {
      setup()
    }
  }
  
  func setup() {
    //if self.subviews.count == 0 {
    view = loadViewFromNib()
    view.frame = bounds
    view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
    addSubview(view)
    //}
  }
  
  func loadViewFromNib() -> UIView {
    let bundle = NSBundle(forClass:self.dynamicType)
    let nib = UINib(nibName: "ProductListHeader", bundle: bundle)
    let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
    return view
  }

}
