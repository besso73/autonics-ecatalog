//
//  LeftMenuViewController.swift
//  catalog
//
//  Created by developer on 11/3/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

class MenuTableViewController : UITableViewController {
  
  var selectedMenuItem : Int = 0
  
  override func viewDidLoad() {
    //print("call viewDidLoad")
    super.viewDidLoad()
    
    // Customize apperance of table view
    tableView.contentInset = UIEdgeInsetsMake(64.0, 0, 0, 0) //
    tableView.separatorStyle = .None
    tableView.backgroundColor = UIColor.clearColor()
    tableView.scrollsToTop = false
    
    // Preserve selection between presentations
    self.clearsSelectionOnViewWillAppear = false
    
    tableView.selectRowAtIndexPath(NSIndexPath(forRow: selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // Return the number of sections.
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // Return the number of rows in the section.
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    let userId = userDefaults.stringForKey("userId")
    
    //print("userId in tableView : \(userId)")
    
    if userId == nil {
      return 7
    }
    
    return 5
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    

    
    var cell = tableView.dequeueReusableCellWithIdentifier("CELL")
    
    if (cell == nil) {
      cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CELL")
      cell!.backgroundColor = UIColor.clearColor()
      cell!.textLabel?.textColor = UIColor.darkGrayColor()
      let selectedBackgroundView = UIView(frame: CGRectMake(0, 0, cell!.frame.size.width, cell!.frame.size.height))
      selectedBackgroundView.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.2)
      cell!.selectedBackgroundView = selectedBackgroundView
    }
    
    var title:String?
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    let userId = userDefaults.stringForKey("userId")
    
    if userId == nil {
      
      if indexPath.row == 0 {
        title = "Category"
      } else if indexPath.row == 1 {
        title = "Sign up"
      } else if indexPath.row == 2 {
        title = "Sign in"
      } else if indexPath.row == 3 {
        title = "Sign out"
      } else if indexPath.row == 4 {
        title = "Forgot Password"
      } else if indexPath.row == 5 {
        title = "Quote"
      } else if indexPath.row == 6 {
        title = "About Autonics"
      }
      

      
      
    } else {
      
      if indexPath.row == 0 {
        title = "Category"
      } else if indexPath.row == 1 {
        title = "Sign Out"
      } else if indexPath.row == 2 {
        title = "Forgot Password"
      } else if indexPath.row == 3 {
        title = "Quote"
      } else if indexPath.row == 4 {
        title = "About Autonics"
      }
      
    }
    cell!.textLabel?.text = title
    
    return cell!
  }
  
  override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 50.0
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    

    /*
    if (indexPath.row == selectedMenuItem) {
    return
    }
    */
    
    selectedMenuItem = indexPath.row
    
    //Present new view controller
    let bundle = NSBundle.mainBundle()
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: bundle)
    
    var destViewController : UIViewController
    
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    let userId = userDefaults.stringForKey("userId")
    
    if userId == nil {
    
      switch (indexPath.row) {
      case 0:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("CategoryViewController")
        break
      case 1:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SignupViewController")
        break
      case 2:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SigninViewController")
        break
      case 3: //Sign out
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("CategoryViewController")
        break
        
      case 4:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ForgotPasswordViewController")
        break
      case 5:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("QuoteViewController")
        break
      case 6:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("AboutVC") as! AboutViewController
        
        break
      default:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("CategoryViewController")
        break
      }
    
      
    } else {
      switch (indexPath.row) {
      case 0:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("CategoryViewController")
        break
      case 1: //Sign out
        NSUserDefaults.standardUserDefaults().removeObjectForKey("userFirstName")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("userId")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        /*
        let myAlert = UIAlertController(title: "Signed Out", message: "", preferredStyle: UIAlertControllerStyle.Alert);
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:nil)
        myAlert.addAction(okAction);
        self.presentViewController(myAlert, animated: true, completion: nil)
        */
        
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("CategoryViewController")
        break
      case 2:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ForgotPasswordViewController")
        break
        
      case 3:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("QuoteViewController")
        break
      case 4:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("AboutVC") as! AboutViewController
        
        break
      default:
        destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("CategoryViewController")
        break
      }

      
    }
      
    sideMenuController()?.setContentViewController(destViewController)
    
  }
  
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
  // Get the new view controller using [segue destinationViewController].
  // Pass the selected object to the new view controller.
  }
  */
  
  /*
  func foo() {
    self.tableView.reloadData()
    
  }
  */
  
}