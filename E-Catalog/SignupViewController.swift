//
//  SignupViewController.swift
//  E-Catalog
//
//  Created by developer on 11/6/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

class SignupViewController : UIViewController {
  
  @IBOutlet weak var userEmailAddressTextField: UITextField!
  @IBOutlet weak var userPasswordTextField: UITextField!
  @IBOutlet weak var userPasswordRepeatTextField: UITextField!
  @IBOutlet weak var userFirstNameTextField: UITextField!
  @IBOutlet weak var userLastNameTextField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func cancelButtonTapped(sender: AnyObject) {
    
    self.dismissViewControllerAnimated(true, completion: nil)
  }

  
  @IBAction func signUpButtonTapped(sender: AnyObject) {

    let userEmail = userEmailAddressTextField.text
    let userPassword = userPasswordTextField.text
    let userPasswordRepeat = userPasswordRepeatTextField.text
    let userFirstName = userFirstNameTextField.text
    let userLastName = userLastNameTextField.text
    
    if( userPassword != userPasswordRepeat)
    {
      // Display alert message
      displayAlertMessage("Error", userMessage: "Passwords do not match")
      return
    }
    
    if(userEmail!.isEmpty || userPassword!.isEmpty || userFirstName!.isEmpty || userLastName!.isEmpty)
    {
      // Display an alert message
      displayAlertMessage("Errir", userMessage: "All fields are required to fill in")
      return
    }
    
    let spinningActivity = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    spinningActivity.labelText = "Loading"
    spinningActivity.detailsLabelText = "Please wait"
    
    // Send HTTP POST
    // TODO. move constants as one header file.
    let sRoute = "feed/rest_api/signup"
    let sKey = "tcTznFpxjTAW8vnq"
    let params = "?route=\(sRoute)&key=\(sKey)&email=\(userEmail!)&password=\(userPassword!)&firstname=\(userFirstName!)&lastname=\(userLastName!)"
    
    let sUrl = "http://autonics-ec.dev/index.php" + params
    print(sUrl)
    let oUrl = NSURL(string: sUrl)!

    NSURLSession.sharedSession().dataTaskWithURL(oUrl, completionHandler: { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
      
      dispatch_async(dispatch_get_main_queue())
        {
          
          spinningActivity.hide(true)
          
          if error != nil {
            self.displayAlertMessage("Error", userMessage: error!.localizedDescription)
            return
          }
          
          do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary

            if let parseJSON = json {

              let userId = parseJSON["userId"] as! Int
              
              print("A : \(parseJSON)")
              
              
              if( userId != 0 ) {
                
                
                let userDefaults = NSUserDefaults.standardUserDefaults()
                
                userDefaults.setObject(parseJSON["userFirstName"], forKey: "userFirstName")
                userDefaults.setObject(parseJSON["userEmail"], forKey: "userEmail")
                userDefaults.setObject(parseJSON["userId"], forKey: "userId")
                userDefaults.synchronize()
                
                // TODO. after message call back
//                let myAlert = UIAlertController(title: "Alert", message: "Registration successful", preferredStyle: UIAlertControllerStyle.Alert);
//                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){(action) in
//                  self.dismissViewControllerAnimated(true, completion: nil)
//                }
//                myAlert.addAction(okAction);
//                self.presentViewController(myAlert, animated: true, completion: nil)
                
                //self.displayAlertMessage("Success",userMessage: "")
                
                // take user to a protected page
                let bundle = NSBundle.mainBundle()
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: bundle)
                
                let mainPage = mainStoryboard.instantiateViewControllerWithIdentifier("CategoryViewController")
                
                self.sideMenuController()?.setContentViewController(mainPage)
                self.toggleSideMenuView()

              } else {

                let errorMessage = parseJSON["message"] as? String
                
                if(errorMessage != nil) {
                  self.displayAlertMessage("Error", userMessage: errorMessage!)
                }
                
              }
              
            }

          } catch{
            print(error)
          }
          
          
          
      }
      
    }).resume()
    
    
  }
  
  func displayAlertMessage(sTitle:String, userMessage:String)
  {
    let myAlert = UIAlertController(title: sTitle, message:userMessage, preferredStyle: UIAlertControllerStyle.Alert);
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:nil)
    myAlert.addAction(okAction);
    self.presentViewController(myAlert, animated: true, completion: nil)
  }

  
  
  @IBAction func menuTapped(sender: AnyObject) {
    toggleSideMenuView()
    
  }
}
