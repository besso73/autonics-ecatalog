//
//  MenuNavigationController.swift
//  E-Catalog
//
//  Created by developer on 11/4/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

class MenuNavigationController: ENSideMenuNavigationController, ENSideMenuDelegate {
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    sideMenu = ENSideMenu(sourceView: self.view, menuViewController: MenuTableViewController(), menuPosition:.Right)
    //sideMenu?.delegate = self //optional
    sideMenu?.menuWidth = 180.0 // optional, default is 160
    //sideMenu?.bouncingEnabled = false
    
    // make navigation bar showing over side menu
    view.bringSubviewToFront(navigationBar)

    // besso add to delegate
    self.sideMenuController()?.sideMenu?.delegate = self;

  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - ENSideMenu Delegate
  func sideMenuWillOpen() {
    print("sideMenuWillOpen")
    
  }
  
  func sideMenuWillClose() {
    print("sideMenuWillClose")
  }
  
  func sideMenuDidClose() {
    print("sideMenuDidClose")
  }
  
  func sideMenuDidOpen() {
    print("sideMenuDidOpen")
  }
}
